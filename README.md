# Спектрограмма
## Что это?
Это задание 5 для конкурса Yandex Mobile Contest (2016).
https://ymc.yandex.ru/
## Текст задания?
С давних времён хижину на окраине деревни называют домом с привидениями. Немногие отваживались зайти туда, а кто заходил — или не возвращались вовсе, или всю жизнь после этого мучились ночными кошмарами, а рассказать ничего не могли. Однажды решилась разобраться с домом команда смельчаков. Внутрь они лезть не стали, однако подобрались близко и сумели записать доносящиеся из дома странные звуки. Записать записали, а проанализировать не смогли — требуется ваша помощь. Необходимо реализовать для команды смельчаков звуковой анализатор, который поможет разгадать загадку, не подвергаясь опасности.
Учёные, слышавшие запись, говорят, что некий тайный смысл может иметь её спектрограмма, но толком ничего объяснить не могут.
## Фуу! Простыня кода!
Да. Мне стыдно.
## А где логарифмические шкалы?
Их нет.
## Почему DecodingStream декодирует при создании, а не при запросе свежих данных?
Потому что документация говорит:
While you are not required to resubmit/release buffers immediately to the codec, holding onto input and/or output buffers may stall the codec, and this behavior is device dependent. Specifically, it is possible that a codec may hold off on generating output buffers until all outstanding buffers have been released/resubmitted. Therefore, try to hold onto to available buffers as little as possible. 

