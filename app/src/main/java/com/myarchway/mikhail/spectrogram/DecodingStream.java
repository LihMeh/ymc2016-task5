package com.myarchway.mikhail.spectrogram;

import android.media.AudioFormat;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.io.TarsosDSPAudioInputStream;

/**
 * Created by lihmeh on 10/27/16.
 */

public class DecodingStream implements TarsosDSPAudioInputStream {

    // https://android.googlesource.com/platform/cts/+/jb-mr2-release/tests/tests/media/src/android/media/cts/DecoderTest.java

    MediaExtractor mediaExtractor;
    MediaCodec codec;

    byte[] decodedData;
    int position = 0;

    private TarsosDSPAudioFormat audioFormat;

    public DecodingStream(MediaExtractor mediaExtractor) throws Exception {
        this.mediaExtractor = mediaExtractor;
        mediaExtractor.selectTrack(0);
        String mime = mediaExtractor.getTrackFormat(0).getString(MediaFormat.KEY_MIME);
        MediaCodec codec = MediaCodec.createDecoderByType(mime);
        codec.configure(mediaExtractor.getTrackFormat(0),null,null,0);

        MediaFormat outFormat = null;

        codec.start();





        boolean good = true;
        int timeoutUs = 5000;
        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
        boolean sawInputEOS = false;
        boolean sawOutputEOS = false;
        int noOutputCounter = 0;
        ByteArrayOutputStream decodedDataCollector = new ByteArrayOutputStream();
        ByteBuffer[] codecInputBuffers = codec.getInputBuffers();
        ByteBuffer[] codecOutputBuffers = codec.getOutputBuffers();
        byte[] copyArray = new byte[0];


        while (!sawOutputEOS && noOutputCounter < 50) {
            noOutputCounter++;

            if (!sawInputEOS) {
                int inputBufferId = codec.dequeueInputBuffer(timeoutUs);
                if (inputBufferId>=0) {
                    int sampleSize = mediaExtractor.readSampleData(codecInputBuffers[inputBufferId],0);
                    long presentationTimeUs = 0;
                    if (sampleSize<0) {
                        sawInputEOS = true;
                        sampleSize = 0;
                    } else {
                        presentationTimeUs = mediaExtractor.getSampleTime();
                    }
                    codec.queueInputBuffer(inputBufferId,0,sampleSize,presentationTimeUs,sawInputEOS ? MediaCodec.BUFFER_FLAG_END_OF_STREAM : 0);
                    if (!sawInputEOS) {
                        mediaExtractor.advance();
                    }
                }
            }

            int outputBufferId = codec.dequeueOutputBuffer(bufferInfo,timeoutUs);

            if (outputBufferId >=0 ) {
                if (outFormat==null) {
                    outFormat = codec.getOutputFormat();
                }

                if (bufferInfo.size >0 ) {
                    noOutputCounter = 0;

                    if (copyArray.length< bufferInfo.size) {
                        copyArray = new byte[bufferInfo.size];
                    }
                    codecOutputBuffers[outputBufferId].get(copyArray,0,bufferInfo.size);
                    decodedDataCollector.write(copyArray, 0, bufferInfo.size);
                }

                codec.releaseOutputBuffer(outputBufferId,false);

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    sawOutputEOS = true;
                }

            } else if (outputBufferId == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                codecOutputBuffers = codec.getOutputBuffers();
            } else if (outputBufferId == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                outFormat = codec.getOutputFormat();
            } else {
            }

        }
        codec.stop();
        codec.release();
        mediaExtractor.release();


        int sampleRate = outFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
        int channelCount = outFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
        /*
        https://developer.android.com/reference/android/media/MediaCodec.html  говорит:
        Each sample is a 16-bit signed integer in native byte order.
        Так что, полагаемся на него...
         */
        audioFormat = new TarsosDSPAudioFormat(sampleRate, 16 /*bits*/, channelCount, true, ByteOrder.nativeOrder()==ByteOrder.BIG_ENDIAN);           // TODO double-check params!!!


        decodedData = decodedDataCollector.toByteArray();
        decodedDataCollector.close();


    }

    @Override
    public int read(byte[] outBuffer, int outBufferOffset, int bytesToCopy) throws IOException {
        int bytesLeft = decodedData.length - position;
        if (bytesLeft < bytesToCopy) bytesToCopy = bytesLeft;

        System.arraycopy(decodedData,position,outBuffer,outBufferOffset,bytesToCopy);

        position += bytesToCopy;
        return bytesToCopy;
    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public TarsosDSPAudioFormat getFormat() {
        return audioFormat;
    }


    @Override
    public long skip(long l) throws IOException {
        throw new IOException("Can not skip in audio stream");
    }

    @Override
    public long getFrameLength() {
        return -1;
    }
}
