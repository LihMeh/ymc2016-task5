package com.myarchway.mikhail.spectrogram;

import android.app.Activity;
import android.app.Fragment;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


public class MediaListFragment extends ListFragment implements
        LoaderManager.LoaderCallbacks<Cursor>{

    private final static int LOADER_ID = 135;

    private final String[] QUERY_FIELDS = {MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DISPLAY_NAME};
    private final String[] ADAPTER_FIELDS = {MediaStore.Audio.Media.DISPLAY_NAME};
    private final int[] ADAPTER_IDS = {android.R.id.text1};

    private ItemClickListener mListener;
    SimpleCursorAdapter cursorAdapter;

    public MediaListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cursorAdapter = new SimpleCursorAdapter(getActivity(),android.R.layout.simple_list_item_1,null,ADAPTER_FIELDS, ADAPTER_IDS, 0);
        this.setListAdapter(cursorAdapter);
        getLoaderManager().initLoader(LOADER_ID,null,this);
    }

    @Override
    public void onDestroy() {
        getLoaderManager().destroyLoader(LOADER_ID);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_media_list, container, false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (mListener != null) {
            mListener.onListItemClick(id);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ItemClickListener) {
            mListener = (ItemClickListener) context;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ItemClickListener) {
            mListener = (ItemClickListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id!=LOADER_ID) return null;
        return new CursorLoader(getActivity(),MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,QUERY_FIELDS,null,null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        cursorAdapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        cursorAdapter.changeCursor(null);
    }

    public interface ItemClickListener {
        // TODO: Update argument type and name
        void onListItemClick(long itemId);
    }
}
