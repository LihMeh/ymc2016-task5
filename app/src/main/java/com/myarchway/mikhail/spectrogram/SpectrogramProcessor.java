package com.myarchway.mikhail.spectrogram;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaExtractor;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import be.tarsos.dsp.io.TarsosDSPAudioFloatConverter;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.util.fft.FFT;


/*
Строит спектрограммы
 */
public class SpectrogramProcessor {

    enum StatusCode {
        OK,
        CANT_OPEN_FILE,
        INCORRECT_TRACK,
        READ_ERROR,
        OTHER_ERROR
    }

    public static class Result {
        public StatusCode statusCode;
        public Bitmap bitmap;
        public Result(StatusCode statusCode) { this.bitmap = null; this.statusCode = statusCode; }
        public Result(Bitmap bitmap) {this.bitmap = bitmap; this.statusCode = StatusCode.OK;}
    }

    /** Строит спектрогармму по аудиозаписи
     * @param resourceId ID ресурса аудиофайла
     * @param bitmapHeight Высота картинки, которую хотим получить. Ширина будет определяться автоматически
     * @param blocksPerSecond количество блоков в секунде (и, естественно, пиклесей)
     * @param logFrequencyScale шкала частот должна быть логарифмической
     * @param context Контакст (нужен для чтения ресурса)
     */
    public static Result getSpectrogram(long resourceId, int bitmapHeight, int blocksPerSecond, boolean logFrequencyScale, boolean logValues, Context context) {


        MediaExtractor mediaExtractor = openFile(resourceId,context);
        if (mediaExtractor==null) return new Result(StatusCode.CANT_OPEN_FILE);
        if (mediaExtractor.getTrackCount()!=1) {
            mediaExtractor.release();
            return new Result(StatusCode.INCORRECT_TRACK);
        }

        DecodingStream inputStream = null;
        try {

            inputStream = new DecodingStream(mediaExtractor);

            TarsosDSPAudioFormat format = inputStream.getFormat();

            // add more steps :)
            TarsosDSPAudioFloatConverter converter = TarsosDSPAudioFloatConverter.getConverter(format);

            /*
            тут мы пытаемся понять,  блоки какого размера надо читать и отдавать в FFT.
             */
            int byteBuffSize = (int) format.getFrameRate()*format.getFrameSize()/blocksPerSecond;
            int floatBuffSize = byteBuffSize/2;

            List<float[]> spectrogramData = new ArrayList<float[]>();

            FFT fft = new FFT(floatBuffSize);

            byte[] byteBuffer = new byte[byteBuffSize];
            float[] floatBuffer = new float[floatBuffSize];
            boolean goon = true;
            while(true) {
                int bytesRead = inputStream.read(byteBuffer,0,byteBuffSize);
                if (bytesRead<=0) break;
                converter.toFloatArray(byteBuffer,floatBuffer,bytesRead/2);
                fft.forwardTransform(floatBuffer);


                // И так, у нас есть массив размером floatBuffer
                // Надо его данные "сжать" в массив размером bitmapHeight.
                // только в этот размер мы покажем не весь диапазон частот, а лишь partOfSpectrum процентов
                // то есть, у нас есть floatBuffer частот. Мы хотим получить bitmapHeight групп частот. В каждую группу я всуну среднее значение
                float[] outArray = new float[bitmapHeight];
                if (logFrequencyScale) {
                    int currentGroup = 0;
                    float currentSum = 0;
                    double maxGroupLog = Math.log10(floatBuffSize+1);
                    double freqWidth = maxGroupLog / bitmapHeight;
                    double nextGroupBorder = freqWidth;
                    for (int i=0;i<floatBuffSize;i++) {
                        if (Math.log10(i+1)<nextGroupBorder) {
                            currentSum += floatBuffer[i];
                        }
                        else {
                            outArray[currentGroup] = currentSum;
                            currentSum = 0;
                            nextGroupBorder += freqWidth;
                            currentGroup++;
                        }
                    }
                }else {
                    int compactRatio = floatBuffSize / bitmapHeight;
                    int currentGroup = 0;
                    while (currentGroup < bitmapHeight) {
                        float sum = 0;
                        int currentPos = currentGroup * compactRatio;
                        int border = currentPos + compactRatio;
                        if (border > floatBuffSize) {
                            compactRatio -= (border - floatBuffSize);
                            border = floatBuffSize;
                        }
                        while (currentPos < border) {
                            sum += floatBuffer[currentPos];
                            currentPos++;
                        }
                        outArray[currentGroup] = sum;// sum / compactRatio;
                        currentGroup++;
                    }
                }
                spectrogramData.add(outArray);

            }

            return new Result(genBitmap(spectrogramData, logValues));



        } catch (IOException ioex) {
            return new Result(StatusCode.READ_ERROR);
        }
        catch (Exception otherex) {
            otherex.printStackTrace();
            return new Result(StatusCode.OTHER_ERROR);
        }
        finally {
            mediaExtractor.release();
            if (inputStream!=null) {
                try {inputStream.close();} catch(IOException ex2) {}
            }
        }

    }

    private static MediaExtractor openFile(long resourceId, Context context) {

        try {
            ParcelFileDescriptor fd = context.getContentResolver().openFileDescriptor(ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, resourceId), "r");
            MediaExtractor mediaExtractor = new MediaExtractor();
            mediaExtractor.setDataSource(fd.getFileDescriptor());
            return mediaExtractor;
        }catch (Exception ex) {
            return null;
        }

    }

    private static Bitmap genBitmap(List<float[]> data, boolean logValues) {
        float maxValue = Float.MIN_VALUE;
        for(float[]col : data) {
            for (int rowNum =0; rowNum<col.length;rowNum++) {
                float abs = Math.abs( col[rowNum] );
                if (abs>maxValue) maxValue = abs;
            }
        }

        int width = data.size();
        int height = data.get(0).length;
        Bitmap bitmap = Bitmap.createBitmap(width,height, Bitmap.Config.RGB_565);

        for (int col=0;col<data.size();col++) {
            float[] colData = data.get(col);
            for (int row=0;row<height;row++) {
                int val = (int) (Math.abs(colData[row])*255/maxValue);
                int color = 0;
                if (colData[row]>=0) {
                    color = Color.rgb(0, val, 0);
                } else {
                    color = Color.rgb(0, 0, val);
                }
                bitmap.setPixel(col,row,color);
            }
        }

        return bitmap;
    }

}
