package com.myarchway.mikhail.spectrogram;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity implements  MediaListFragment.ItemClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onListItemClick(long itemId) {
        Intent intent = new Intent(this,SpectrogramActivity.class);
        intent.putExtra(SpectrogramActivity.PARAM_MEDIA_ID,itemId);
        startActivity(intent);
    }
}
