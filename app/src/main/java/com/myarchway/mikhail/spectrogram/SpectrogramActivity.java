package com.myarchway.mikhail.spectrogram;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

public class SpectrogramActivity extends Activity {

    public static final String PARAM_MEDIA_ID = "media_id";

    boolean startProcessing = true;
    Bitmap showingBitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spectrogram);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (startProcessing) {
            startProcessing = false;
            View imageView = findViewById(R.id.imageView);
            imageView.measure(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
            int height = (int)(imageView.getMeasuredHeight() * getResources().getDisplayMetrics().density);
            long resourceId = getIntent().getLongExtra(PARAM_MEDIA_ID,0);
            (new ProcessImageTask()).execute(resourceId, (long)height);
        }
    }

    private class ProcessImageTask extends AsyncTask<Long,Object,SpectrogramProcessor.Result> {

        @Override
        protected SpectrogramProcessor.Result doInBackground(Long... params) {
            return SpectrogramProcessor.getSpectrogram(params[0], params[1].intValue(), 100, false, false, SpectrogramActivity.this);
        }

        @Override
        protected void onPostExecute(SpectrogramProcessor.Result result) {
            if (result==null) {
                Toast.makeText(SpectrogramActivity.this,"Empty result O_o",Toast.LENGTH_SHORT).show();
                return;
            }

            if (result.statusCode!= SpectrogramProcessor.StatusCode.OK) {
                Toast.makeText(SpectrogramActivity.this,"Error :(",Toast.LENGTH_SHORT).show();
                return;
            }

            showingBitmap = result.bitmap;

            ImageView imageView = ((ImageView)findViewById(R.id.imageView));
            imageView.setImageBitmap(showingBitmap);

        }

    }



}
